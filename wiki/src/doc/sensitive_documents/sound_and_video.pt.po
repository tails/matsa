# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2014-07-14 13:08+0200\n"
"PO-Revision-Date: 2012-07-30 06:34-0000\n"
"Last-Translator: lucas alkaid <alkaid@ime.usp.br>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: Portuguese\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "[[!meta title=\"Audio\"]]\n"
msgid "[[!meta title=\"Sound and video\"]]\n"
msgstr "[[!meta title=\"Audio\"]]\n"

#. type: Plain text
msgid "Tails includes several sound and video applications:"
msgstr ""

#. type: Bullet: '  - '
#, fuzzy
#| msgid ""
#| "[Audacity](http://audacity.sourceforge.net) is a multi-track audio editor "
#| "for Linux/Unix, MacOS and Windows. It is designed for easy recording, "
#| "playing and editing of digital audio."
msgid ""
"**<span class=\"application\">[Audacity](http://audacity.sourceforge.net/)</"
"span>** is a multi-track audio editor designed for recording, playing and "
"editing of digital audio.  See the [official documentation](http://manual."
"audacityteam.org/o/)."
msgstr ""
"[Audacity](http://audacity.sourceforge.net) é um editor de áudio multipista "
"para Linux/Unix, MacOS e Windows. Ele é designado para fácil gravação, "
"reprodução e edição de áudio digital."

#. type: Bullet: '  - '
msgid ""
"**<span class=\"application\">[Brasero](http://projects.gnome.org/brasero/)</"
"span>** is an application to burn, copy, and erase CD and DVD media: audio, "
"video, or data.  See the [official documentation](https://help.gnome.org/"
"users/brasero/stable/)."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"**<span class=\"application\">[Pitivi](http://www.pitivi.org/)</span>** is a "
"video editor.  See the [official documentation](http://www.pitivi.org/"
"manual/)."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"**<span class=\"application\">[Sound Juicer](http://burtonini.com/blog/"
"computers/sound-juicer)</span>** is a CD ripper application."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"**<span class=\"application\">[Traverso](http://traverso-daw.org/)</span>** "
"is a multi-track audio recorder and editor."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"These applications can be started from the\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Sound & Video</span></span> menu.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/sensitive_documents/persistence\" raw=\"yes\"]]\n"
msgstr ""
