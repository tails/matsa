# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2014-09-22 19:49+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Installing onto a USB stick or SD card\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Tails includes <span class=\"application\">Tails Installer</span>: a custom\n"
"installer for USB sticks and SD cards.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>Using <span class=\"application\">Tails Installer</span> allows you to\n"
"later [[create a persistent volume|persistence]] in the free space\n"
"left on the device.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"note\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p><span class=\"application\">Tails Installer</span> can only install Tails on a\n"
"USB stick or SD card of <strong>at least 4 GB</strong>.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"For the moment, <span class=\"application\">Tails Installer</span> is only\n"
"available from inside Tails. So you need to start Tails from a first\n"
"media, and later clone it onto the device of your choice, USB stick or SD card.\n"
msgstr ""

#. type: Bullet: '1. '
msgid "Get a first Tails running. To do so you can either:"
msgstr ""

#. type: Bullet: '   - '
msgid "Start Tails from a [[Tails DVD|dvd]] (recommended)."
msgstr ""

#. type: Bullet: '   - '
msgid ""
"Start Tails from another Tails USB stick or SD card, for example from a "
"friend."
msgstr ""

#. type: Bullet: '   - '
msgid ""
"[[Manually install Tails onto another USB or SD card|installation/manual]] "
"and start *Tails Installer* from it."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"2. Choose\n"
"   <span class=\"menuchoice\">\n"
"     <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"     <span class=\"guisubmenu\">Tails</span>&nbsp;▸\n"
"     <span class=\"guimenuitem\">Tails Installer</span>\n"
"   </span>\n"
"   to start <span class=\"application\">Tails Installer</span>.\n"
msgstr ""

#. type: Bullet: '3. '
msgid ""
"To install onto a new device, click on the <span class=\"button\">Clone & "
"Install</span> button."
msgstr ""

#. type: Bullet: '4. '
msgid "Plug the device onto which you want to install Tails."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   A new device, which corresponds to the USB stick or SD card, appears in the\n"
"   <span class=\"guilabel\">Target Device</span> drop-down list.\n"
msgstr ""

#. type: Bullet: '5. '
msgid ""
"Choose this new device from the <span class=\"guilabel\">Target Device</"
"span> drop-down list."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   <div class=\"caution\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <ul>\n"
"   <li><strong>All the data on the installed device will be\n"
"   lost.</strong></li>\n"
"   <li><strong>This operation does not [[securely\n"
"   delete|encryption_and_privacy/secure_deletion]] the lost data on the\n"
"   installed device.</strong></li>\n"
"   <li><strong>This operation does not copy the persistent volume of the\n"
"   device which is being cloned.</strong></li>\n"
"   </ul>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   </div>\n"
msgstr ""

#. type: Bullet: '6. '
msgid ""
"To start the installation, click on the <span class=\"button\">Install "
"Tails</span> button."
msgstr ""

#. type: Bullet: '7. '
msgid ""
"Read the warning message in the pop-up window. Click on the <span class="
"\"button\">Yes</span> button to confirm."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"next\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>After the installation completes, you can [[start Tails|start_tails]]\n"
"from this new device.</p>\n"
msgstr ""
