[[!meta title="Agenda for the next contributors meeting"]]

[[Official place and date are in the calendar.|contribute/calendar]]

Availability and plans for the next weeks
=========================================

  - Volunteers to handle "[Hole in the roof](https://labs.riseup.net/code/versions/198)" tickets this month
  - Availability and plans for monthly low-hanging fruits meeting

Discussions
===========

 - [[!tails_ticket 8586 desc="Create a directory of organizations doing Tails training"]]
 - [[!tails_ticket 8832 desc="Consider removing signature from Torrent"]]
 - [[!tails_ticket 8931 desc="Decide if we want to do the verification in the website or in the add-ons menus"]]
 - [[!tails_ticket 8872 desc="Decide which kind of verification would the ISO verification extension do"]]
