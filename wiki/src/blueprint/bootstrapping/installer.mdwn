[[!meta title="Tails Installer"]]

Vision
======

Installing Tails onto a USB stick is probably the most common and
recommended scenario when started with Tails as this allows incremental
upgrades and persistence.

But as of 2014, getting Tails installed on a USB stick requires having a
first temporary Tails to boot from and then running Tails Installer.
Also, due to the way Tails Installer partitions the USB stick, this
requires having two USB sticks and going through manual installation
steps, often using command line.

We want to eliminate the need for a first temporary Tails and have Tails
Installer available on Linux, Windows, and Mac OS.

As part of the ISO verification process we also want to push more
verification logic to Tails Installer. See the [[blueprint on ISO
verification|verification]].

Tails Installer is also used to do full upgrades. We want to move this
to Tails Upgrader. See the [[blueprint on Tails Upgrader|upgrade]].

Roadmap
=======

2015
----

In 2015 we will work on porting Tails Installer to Debian. This is a
first step before having Tails available on other platform. This
requires rethinking the scenarios in which Tails Installer is used
([[!tails_ticket 7046]]) and adapting its interface accordingly.

Those are the improvements that we propose for 2015:

- Remove the splash screen:
  - Outside of Tails
  - In Tails, add a "Clone" button to the main interface
- Implement "Install from ISO".
- Autodetect if the destination key has Tails already.
  - Then display "Install" and "Upgrade" buttons accordingly.
- Make it possible to do "Install from ISO" and "Upgrade from ISO" from
  the command line. See the [[Debian Hacker|bootstrapping#tools]] bootstrapping path.
- Rework the wording of the main interface.
- Add visual and textual context to main interface.
- Point to website to download ISO if outside of Tails.
- Add second button "Upgrade" below or side-by-side with "Install"
  button.
- Add a splash about creating persistence after rebooting.

This will have to be coordinated with the Ubuntu release calendar.
Ubuntu 15.10 is planned to be freezed in August 2015.

Bonus for 2015
--------------

The following improvements would be nice addition to the roadmap for
2015, if possible:

- Store version of Tails on destination key after install and upgrade.
  - This would allow to display version in "Target Device".
- Have Tails Installer available on Mac as it seems hard to find a
  graphical installation technique.

Future
------

- Push more ISO verification logic pushed to Tails Installer. See the
  [[blueprint on ISO verification|verification]].
