# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2013-06-25 10:51+0300\n"
"PO-Revision-Date: 2013-06-16 09:50+0200\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#. type: Content of: outside any tag (error?)
msgid ""
"[[!meta date=\"2013-06-15 14:45:00\"]] [[!meta title=\"Call for testing: "
"0.19~rc1\"]]"
msgstr ""
"[[!meta date=\"2013-06-15 14:45:00\"]] [[!meta title=\"Appel à tester Tails "
"0.19~rc1\"]]"

#. type: Content of: <p>
msgid ""
"You can help Tails! The first (and hopefully only) release candidate for the "
"upcoming version 0.19 is out. Please test it and see if all works for you."
msgstr ""
"Vous pouvez aider Tails ! La première (et on espère seule) version candidate "
"pour la version 0.19 à venir est sortie. Merci de la tester et de voir si "
"tout fonctionne pour vous."

#. type: Content of: outside any tag (error?)
msgid "[[!toc levels=1]]"
msgstr ""

#. type: Content of: <h1>
msgid "How to test Tails 0.19~rc1?"
msgstr "Comment tester Tails 0.19~rc1 ?"

#. type: Content of: <ol><li><p>
msgid ""
"<strong>Keep in mind that this is a test image.</strong> We have made sure "
"that it is not broken in an obvious way, but it might still contain "
"undiscovered issues."
msgstr ""
"Gardez à l'esprit que c'est une image de test. Nous nous sommes assurés "
"qu'elle n'est pas corrompue d'une manière évidente, mais elle peut toujours "
"contenir des problèmes non découverts."

#. type: Content of: <ol><li><p>
msgid "Download the ISO image and its signature:"
msgstr "Téléchargez l'image ISO et sa signature :"

#. type: Content of: <ol><li><p>
msgid ""
"<a class=\"download-file\" href=\"http://dl.amnesia.boum.org/tails/testing/"
"tails-i386-0.19-rc1/tails-i386-0.19-rc1.iso\" >Tails 0.19~rc1 ISO image</a>"
msgstr ""
"<a class=\"download-file\" href=\"http://dl.amnesia.boum.org/tails/testing/"
"tails-i386-0.19-rc1/tails-i386-0.19-rc1.iso\" >Image ISO de Tails 0.19~rc1</"
"a>"

#. type: Content of: <ol><li><p>
msgid ""
"<a class=\"download-signature\" href=\"http://dl.amnesia.boum.org/tails/"
"testing/tails-i386-0.19-rc1/tails-i386-0.19-rc1.iso.pgp\" >Tails 0.19~rc1 "
"signature</a>"
msgstr ""
"<a class=\"download-signature\" href=\"http://dl.amnesia.boum.org/tails/"
"testing/tails-i386-0.19-rc1/tails-i386-0.19-rc1.iso.pgp\" >Signature de "
"Tails 0.19~rc1</a>"

#. type: Content of: <ol><li><p>
msgid "[[Verify the ISO image|download#verify]]."
msgstr "[[Vérifiez l'image ISO|download#verify]]."

#. type: Content of: <ol><li><p>
msgid ""
"Have a look at the list of <a href=\"#known_issues\">known issues of this "
"release</a> and the list of [[longstanding known issues|support/"
"known_issues]]."
msgstr ""
"Jetez un œil à la liste des <a href=\"#known_issues\">problèmes connus de "
"cette version</a> et à la liste des [[problèmes connus de longue date|"
"support/known_issues]]."

#. type: Content of: <ol><li><p>
msgid "Test wildly!"
msgstr "Testez à volonté !"

#. type: Content of: <p>
msgid ""
"If you find anything that is not working as it should, please [[report to us|"
"doc/first_steps/bug_reporting]]! Bonus points if you check that it is not a "
"<a href=\"#known_issues\">known issue of this release</a> or a "
"[[longstanding known issue|support/known_issues]]."
msgstr ""
"Si vous découvrez quelque chose qui ne fonctionne pas comme prévu, merci de "
"[[nous le rapporter|doc/first_steps/bug_reporting]] ! Points bonus si vous "
"vérifiez que ce n'est pas un <a href=\"#known_issues\">problème connu de "
"cette version</a> ou un [[problème connu de longue date|support/"
"known_issues]]."

#. type: Content of: <h1>
msgid "What's new since 0.18?"
msgstr "Quoi de neuf depuis la 0.18 ?"

#. type: Content of: <ul><li>
msgid "New features"
msgstr "Nouvelles fonctionnalités"

#. type: Content of: <ul><ul><li><p>
msgid "Linux 3.9.5-1."
msgstr "Linux 3.9.5-1."

#. type: Content of: <ul><ul><li><p>
msgid ""
"Unblock Bluetooth, Wi-Fi, WWAN and WiMAX; block every other type of wireless "
"device."
msgstr ""

#. type: Content of: <ul><li>
msgid "Bugfixes"
msgstr "Corrections de bugs"

#. type: Content of: <ul><ul><li><p>
msgid "Fix write access to boot medium at the block device level."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "tails-greeter l10n-related fixes."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid ""
"gpgApplet: partial fix for clipboard emptying after a wrong passphrase was "
"entered."
msgstr ""

#. type: Content of: <ul><li><p>
msgid "Minor improvements"
msgstr "Améliorations mineures"

#. type: Content of: <ul><ul><li><p>
msgid "Drop GNOME proxy settings."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "Format newly created persistent volumes as ext4."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "GnuPG: don't connect to the keyserver specified by the key owner."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "GnuPG: locate keys only from local keyrings."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid ""
"Upgrade live-boot and live-config to the 3.0.x final version from Wheezy."
msgstr ""

#. type: Content of: <ul><li><p>
msgid "Localization: many translation updates all over the place."
msgstr "Langues: de nombreuses mises à jour de traduction un peu partout."

#. type: Content of: <ul><li><p>
msgid "Test suite"
msgstr "Suite de tests"

#. type: Content of: <ul><ul><li><p>
msgid "Re-enable previously disabled boot device permissions test."
msgstr ""

#. type: Content of: <h1>
msgid "<a id=\"known_issues\"></a>Known issues in 0.19~rc1"
msgstr "<a id=\"known_issues\"></a>Problèmes connus dans la version 0.19~rc1"

#. type: Content of: <ul><li>
msgid ""
"Due to a regression in live-config, passwordless sudo is enabled by default "
"unless one sets an administration password."
msgstr ""

#. type: Content of: <ul><li>
msgid "The Unsafe Browser does not work."
msgstr "Le Navigateur web Non-sécurisé ne fonctionne pas."

#. type: Content of: <p>
msgid "All these issues are fixed in Git and will be fixed in Tails 0.19."
msgstr ""
"Tous ces problèmes sont corrigés dans le dépôt Git et le seront dans Tails "
"0.19."
